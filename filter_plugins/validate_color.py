# filter_plugins/validate_color_number.py

from ansible.errors import AnsibleFilterError


def validate_color(value):

   # Check if the value is a string
   if not isinstance(value, str):
      raise AnsibleFilterError("'value' must be a string")

   # Check if the value is a valid color
   valid_colors = ['red', 'green', 'blue', 'yellow']

   if value.lower() not in valid_colors:
      raise AnsibleFilterError("'value' must be a valid color (red, green, blue)")
   
   return f'{ value } is a color'

def validate_num(value):
   # Check if the value is an int
   if not isinstance(value, int):
      raise AnsibleFilterError("'value' must be an integer")

   # Check if the value is a valid number
   try:
      number = int(value)
      if number < 1 or number > 100:
         raise AnsibleFilterError("'value' must be a valid number between 1 and 100")
   except ValueError:
      raise AnsibleFilterError("'value' must be a valid number")
   
   return f"{number} is a an integer"

class FilterModule:
    def filters(self):
      return {
         'validate_color': validate_color,
         'validate_num': validate_num
      }
